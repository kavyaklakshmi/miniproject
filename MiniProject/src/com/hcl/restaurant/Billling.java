package com.hcl.restaurant;

import java.time.LocalDateTime;
import java.util.*;

public class Billling {
	private String name;
	private List<MenuCard> items;
	private double cost;
	private Date time;
	
	public Billling() {}
	
	public Billling(String name, List<MenuCard> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<MenuCard> getItems() {
		
		
		return items;
	}
	public void setItems(List<MenuCard> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}

	
	


}


