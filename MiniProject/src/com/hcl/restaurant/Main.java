package com.hcl.restaurant;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
	
		@SuppressWarnings("deprecation")

		public static void main(String[] args) {
			Scanner scan = new Scanner(System.in);
			Date time=new Date();
			
			List<Billling> bills = new ArrayList<Billling>();
			boolean finalOrder;
			List<MenuCard> items = new ArrayList<MenuCard>();
			MenuCard order1 = new MenuCard(1, "Rice and Samber", 1, 50.0);
			MenuCard order2 = new MenuCard(2, "Masala Dosa", 3, 90.0);
			MenuCard order3 = new MenuCard(3, "Miles", 1, 100.0);
			MenuCard order4 = new MenuCard(4, "Roti and Curry", 2, 120.0);
			MenuCard order5 = new MenuCard(5, "Biryani", 5, 450.0);
			items.add(order1);
			items.add(order2);
			items.add(order3);
			items.add(order4);
			items.add(order5);
			
			System.out.println(
					"***********************<<WELCOME TO GREENVIEW RESTAURANT\n<<****************>>");

			while (true) {
				System.out.println("Please Enter the Login Credentials");

				System.out.println("Email = ");
				String email = scan.next();

				System.out.println("Password = ");
				String password = scan.next();

				String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

				System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
				String adminOrUser = scan.next();

				Billling bill = new Billling();
				List<MenuCard> selectedItems = new ArrayList<MenuCard>();
				
				double totalCost = 0;
				
				Date date=new Date();
				
				ZonedDateTime time1 = ZonedDateTime.now();
				DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E MMM dd  yyyy HH:MM:SS");
				String currentTime = time1.format(dateFormat);

				if (adminOrUser.equals("U") || adminOrUser.equals("u")) {

					System.out.println("Welcome Mr." + name);
					do {
						System.out.println("Today's Menu :- ");
						items.stream().forEach(i -> System.out.println(i));
						System.out.println("Enter the Menu Item Code");
						int code = scan.nextInt();
						
						if (code == 1) {
							selectedItems.add(order1);
							totalCost += order1.getItemPrice();
						}

						else if (code == 2) {
							selectedItems.add(order2);
							totalCost += order2.getItemPrice();
						} else if (code == 3) {
							selectedItems.add(order3);
							totalCost += order3.getItemPrice();
						} else if (code == 4) {
							selectedItems.add(order4);
							totalCost += order4.getItemPrice();
						} else {
							selectedItems.add(order5);
							totalCost += order5.getItemPrice();
						}
						
						System.out.println("Press 0 to show bill\nPress 1 to order more");
						int opt = scan.nextInt();
						if (opt == 0)
							finalOrder = false;
						else
							finalOrder = true;

					} while (finalOrder);
					

					System.out.println("Thanks Mr " + name + " for dining in with continental ");
					System.out.println("Items you have Selected");
					selectedItems.stream().forEach(e -> System.out.println(e));
					System.out.println("Your Total bill will be " + totalCost);

					bill.setName(name);
					bill.setCost(totalCost);
					bill.setItems(selectedItems);
					bill.setTime(date);
					bills.add(bill);

				} else if (adminOrUser.equals("A") || adminOrUser.equals("a")) {
					System.out.println("Welcome Admin");
					System.out.println(
							"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
					int option = scan.nextInt();
					switch (option) {
					case 1:
						if ( !bills.isEmpty()) {
							for ( Billling b : bills) {
								if(b.getTime().getDate()==time.getDate()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills today.!");
						break;

					case 2:
						if (!bills.isEmpty()) {
							for (Billling b : bills) {
								if (b.getTime().getMonth()==time.getMonth()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills for this month.!");
						break;

					case 3:
						if (!bills.isEmpty()) {
							for (Billling b : bills) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total Bill :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						} else
							System.out.println("No Bills registered yet..!");

						break;

					default:
						System.out.println("Invalid Option");
						System.exit(1);
					}
				} else if (adminOrUser.equals("L") || adminOrUser.equals("l")) {
					System.exit(1);
					
				}else  {
					System.out.println("Invalid Entry");
				}

			}

		}

}			

		